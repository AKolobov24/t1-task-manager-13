package ru.t1.akolobov.tm.controller;

import ru.t1.akolobov.tm.api.controller.IProjectTaskController;
import ru.t1.akolobov.tm.api.service.IProjectTaskService;
import ru.t1.akolobov.tm.util.TerminalUtil;

public class ProjectTaskController implements IProjectTaskController {

    private final IProjectTaskService projectTaskService;

    public ProjectTaskController(IProjectTaskService projectTaskService) {
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void bindTaskToProject() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("[ENTER PROJECT ID]");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("[ENTER TASK ID]:");
        final String taskId = TerminalUtil.nextLine();
        projectTaskService.bindTaskToProject(projectId, taskId);
        System.out.println("[OK]");
    }

    @Override
    public void unbindTaskFromProject() {
        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.println("[ENTER TASK ID]:");
        final String taskId = TerminalUtil.nextLine();
        projectTaskService.unbindTaskFromProject(taskId);
        System.out.println("[OK]");
    }
}
