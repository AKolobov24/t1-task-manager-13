package ru.t1.akolobov.tm.api.controller;

import ru.t1.akolobov.tm.model.Task;

import java.util.List;

public interface ITaskController {

    void changeTaskStatusById();

    void changeTaskStatusByIndex();

    void clearTasks();

    void completeTaskById();

    void completeTaskByIndex();

    void createTask();

    void displayTasks();

    void displayTasksByProjectId();

    void displayTask(Task task);

    void displayTaskById();

    void displayTaskByIndex();

    void updateTaskById();

    void updateTaskByIndex();

    void removeTaskById();

    void removeTaskByIndex();

    void renderTaskList(List<Task> taskList);

    void startTaskById();

    void startTaskByIndex();
}
